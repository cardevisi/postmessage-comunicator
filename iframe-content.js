(function(window) {
	window.addEventListener('message', function(event) {
		var pattern = /uol.com.br/;
		if(pattern.test(event.origin)) {
			var data = JSON.parse(event.data);
			for (var key in data) {
				console.log('iframe-receive', key,':', data[key]);
			}
			parent.window.postMessage('iframe-receive-complete', '*');
		}
	});
})(window);