window.addEventListener('message', function(event){
	var pattern = /uol.com.br/;
	if(pattern.test(event.origin)) {
		console.log('event', event.origin, event.data);
	}
});

function loadComplete(element) {
	var trackingValues = {
	    'mute' : '1451496899003',
	    'unmute' : '1451496899003',
	    'resumed' : '1451496899003',
	    'play' : '1451496899003',
	    'paused' : '1451496899003',
	    'firstQuartile' : '1451496899003',
	    'midpoint' : '1451496899003',	
	    'thirdQuartile' : '1451496899003', 	
	    'complete' : '1451496899003'
	};
	element.contentWindow.postMessage(JSON.stringify(trackingValues), '*');
}